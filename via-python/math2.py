def isprime(n):
    if n < 2:
        return False
    elif n == 2:
        return True

    if n % 2 == 0:
        return False

    i = 3
    while i <= n / i:
        if n % i == 0:
            return False
        i += 2

    return True

class Primes(object):

    def __init__(self, begin, length):
        self.__begin = begin
        self.__length = length

    def __iter__(self):
        self.__returned = 0
        self.__current = self.__begin - 1
        return self

    def __next__(self):
        while self.__returned < self.__length:
            self.__current += 1
            if isprime(self.__current):
                self.__returned += 1
                return self.__current

        raise StopIteration
