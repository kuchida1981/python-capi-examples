# README #

使い方と各ファイル・ディレクトリの説明など。


# ディレクトリ

`./via-python` にはPythonのモジュールが置いてあります。

`./via-python-capi` にはCのライブラリのソースと、それをPythonモジュールにする
コードがあります。

`./test` にはPythonのテストコードがおいてあります。
ピュアPythonのモジュールと、C APIで作ったモジュールが同じように動くかの
確認用。


# 動作環境について

Cコンパイラ, Python3, GCC, GNU Make が必要です。
C用のテストコードを動かしたければ, Google test も必要です。

archlinux上で動作確認しています。cygwinでも動くと思いますが、未確認。
Visual Studio環境で動かす自信はあまりありません。

ツールのバージョン。

    % uname -a
    Linux lm750 3.18.2-2-ARCH #1 SMP PREEMPT Fri Jan 9 07:37:51 CET 2015 x86_64 GNU/Linux
    
    % python --version
    Python 3.4.2
    
    % cmake --version
    cmake version 3.1.1
    
    % make --version
    GNU Make 4.1
    
    % gcc --version
    gcc (GCC) 4.9.2 20141224 (prerelease)

cygwinでも動作確認済み。

    CYGWIN_NT-6.1 hi4063 1.7.33-2(0.280/5/3) 2014-11-13 15:47 x86_64 Cygwin
    gcc (GCC) 4.8.3
    GNU Make 4.0
    cmake version 2.8.11.2
    Python 3.2.5


# C APIでPythonモジュールをビルドするまでの手順

math2モジュールをビルドし、C APIでPythonモジュールをビルドし、
使えるようにするまでの手順です。


## math2 (C) ライブラリをビルドする

ビルド用のフォルダを作る

    % mkdir via-python-capi/math2/_build
    % cd via-python-capi/math2/_build

cmakeを実行。

    % cmake -Ddo_gtest=OFF -DCMAKE_C_FLAGS="-fPIC" ..

コンパイルする。エラーが出なければ成功です。

    % make

インストールする。
便宜上(?)、`via-python-capi/local` をprefixにしていますので、
そこにヘッダファイルやライブラリのファイルがコピーされます。

    % make install


## Pythonモジュールをビルドする

カレントディレクトリを変更する。

    % cd via-python-capi

ビルド実行。

    % python setup.py build

`build/lib.${アーキテクチャ名?}` というディレクトリが作られて、
そのなかに `.so` ファイルが作られていたら成功です。
Windowsなら `.pyd` というファイル名かもしれません。


# Pythonモジュールをテストする

カレントディレクトリを変更する

    % cd test

Pythonで書かれたコードを実行する場合は、次のコマンドを実行してください

    % PYTHONPATH=../via-python python -m unittest math2_test

C APIで作ったモジュールをテストするには、次のコマンドを実行してください。

    % PYTHONPATH=../via-python-capi/build/lib.xxxx python -m unittest math2_test

`lib.xxxx` の部分は適宜置き換えてください。
