from distutils.core import setup, Extension
import os

ProjectRoot = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
_ = lambda *pathes: os.path.join(ProjectRoot, '/'.join(pathes))

mod_math2 = Extension('math2',
        include_dirs = [_('usr/local/include')],
        library_dirs = [_('usr/local/lib')],
        libraries = ['math2'],
        sources = ['math2.c'])

mod_sandbox = Extension('sandbox',
        sources = ['sandbox.c'])

setup(name = 'PackageExample',
        version = '1.0',
        description = '',
        author = 'Kosuke Uchida',
        author_email = 'byebyeearthjpn@gmail.com',
        url = 'http://zashikiro.hateblo.jp',
        long_description = '',
        ext_modules = [mod_math2, mod_sandbox])
