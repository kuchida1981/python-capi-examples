#include <Python.h>
#include <stdio.h>

static PyObject* sandbox_SandboxError;

/* ----
 * 可変長引数を扱う
 * ---
 *
 * pythonでやる,
 *
 * def summ(*args):
 *     return sum(args)
 *
 * みたいなやつ
 *
 * 可変長の引数を合計する
 * 数値以外を引数に渡すと TypeError を返す
 */
static PyObject* sandbox_summ(PyObject *self, PyObject *args)
{
	int i;
	long summ = 0;
	PyObject *pyItem;

	Py_ssize_t size = PyTuple_Size(args);
	for(i = 0; i < size; i++)
	{
		pyItem = PyTuple_GetItem(args, i);
		if(!PyLong_Check(pyItem))
		{
			PyErr_SetString(PyExc_TypeError, "int only.");
			return NULL;
		}
		summ += PyLong_AsLong(pyItem);
	}

	return PyLong_FromLong(summ);
}

/* ---
 * 関数を引数とする。リスト・タプルを引数として受け取り、
 * C API モジュールのなかで実行させるサンプル
 * ---
 *
 *  python でいう
 *  map(lambda m, n * m * n, ((1, 2), (2, 3), (3, 4)))
 *  みたいなやつ (2, 6, 12) が返される想定
 *  上のコードでいう lambdaによる関数と list or tuple を受け取る
 *
 * map的な関数の実装
 * mapは返さない. listを渡せばlistを、tupleを渡せばtupleを返す
 * list, tuple以外だととりあえずエラーにしている
 */
static PyObject* sandbox_map(PyObject *self, PyObject *args)
{
	PyObject *pFunc;
	PyObject *pFuncArgs;
	PyObject *pFuncResult;
	PyObject *pItems;
	PyObject *pItem;
	PyObject *pNewItems;
	int i;

	Py_ssize_t size;

	if(!PyArg_ParseTuple(args, "OO", &pFunc, &pItems))
		return NULL;

	if(!PyCallable_Check(pFunc))
	{
		PyErr_SetString(PyExc_TypeError, "pFunc is not callable");
		return NULL;
	}

	if(!PySequence_Check(pItems))
	{
		PyErr_SetString(PyExc_TypeError, "pItems is not sequence");
		return NULL;
	}

	size = PySequence_Size(pItems);
	pNewItems = PyTuple_New(size);

	for(i = 0; i < size; i++)
	{
		pItem = PySequence_GetItem(pItems, i);
		if(PySequence_Check(pItem))
		{
			pFuncArgs = PySequence_Tuple(pItem);
		}
		else
		{
			pFuncArgs = PyTuple_Pack(1, pItem);
		}
		pFuncResult = PyObject_CallObject(pFunc, pFuncArgs);
		if(pFuncResult == NULL)
		{
			PyErr_SetString(PyExc_TypeError, "failed call object.");
			return NULL;
		}
		PyTuple_SetItem(pNewItems, i, pFuncResult);
		
	}

	return pNewItems;
}

static PyMethodDef sandboxMethods[] = {
	{"summ", sandbox_summ, METH_VARARGS, ""},
	{"map", sandbox_map, METH_VARARGS, ""},
	{NULL, NULL, 0, NULL},
};

static struct PyModuleDef sandboxModule = {
	PyModuleDef_HEAD_INIT,
	"sandbox",
	NULL,
	-1,
	sandboxMethods,
};

PyMODINIT_FUNC PyInit_sandbox(void)
{
	PyObject *m;
	m = PyModule_Create(&sandboxModule);
	if(m == NULL)
		return NULL;

	sandbox_SandboxError = PyErr_NewException("sandbox.error", NULL, NULL);
	Py_INCREF(sandbox_SandboxError);
	PyModule_AddObject(m, "error", sandbox_SandboxError);

	return m;
}
