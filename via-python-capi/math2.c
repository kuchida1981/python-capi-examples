#include <Python.h>
#include "structmember.h"
#include <math2.h>

static PyObject* math2_py_isPrime(PyObject *self, PyObject *args)
{
	int n;
	if(!PyArg_ParseTuple(args, "i", &n))
		return NULL;
	return PyBool_FromLong(math2_isPrime(n));
}


typedef struct {
	PyObject_HEAD
	math2_Primes *primes;
} math2_py_Primes;

static void math2_py_Primes_dealloc(math2_py_Primes *self)
{
	PyMem_Free(self->primes);
	Py_TYPE(self)->tp_free((PyObject*)self);
}

static int math2_py_Primes_init(math2_py_Primes *self, PyObject *args, PyObject *kwds)
{
	int begin, length;
	self->primes = (math2_Primes*)PyMem_Malloc(sizeof(math2_Primes));
	if(!PyArg_ParseTuple(args, "ii", &begin, &length))
		return -1;
	math2_Primes_initialize(self->primes, begin, length);
	return 0;
}

static PyObject* math2_py_Primes_iter(PyObject *self)
{
	Py_INCREF(self);
	return self;
}

static PyObject* math2_py_Primes_iternext(PyObject *self)
{
	int value;
	math2_py_Primes *_self = (math2_py_Primes*)self;
	if(math2_Primes_next(_self->primes, &value))
		return PyLong_FromLong(value);
	else
	{
		PyErr_SetNone(PyExc_StopIteration);
		return NULL;
	}
}

static PyTypeObject math2_py_PrimesType = {
	PyVarObject_HEAD_INIT(NULL, 0)
	"math2.Primes",             /* tp_name */
	sizeof(math2_py_Primes), /* tp_basicsize */
	0,                         /* tp_itemsize */
	(destructor)math2_py_Primes_dealloc,                         /* tp_dealloc */
	0,                         /* tp_print */
	0,                         /* tp_getattr */
	0,                         /* tp_setattr */
	0,                         /* tp_reserved */
	0,                         /* tp_repr */
	0,                         /* tp_as_number */
	0,                         /* tp_as_sequence */
	0,                         /* tp_as_mapping */
	0,                         /* tp_hash  */
	0,                         /* tp_call */
	0,                         /* tp_str */
	0,                         /* tp_getattro */
	0,                         /* tp_setattro */
	0,                         /* tp_as_buffer */
	Py_TPFLAGS_DEFAULT,        /* tp_flags */
	"Primes 's docs",           /* tp_doc */

	0,                         /* tp_traverse */
	0,                         /* tp_clear */
	0,                         /* tp_richcompare */
	0,                         /* tp_weaklistoffset */
	math2_py_Primes_iter,                         /* tp_iter */
	math2_py_Primes_iternext,                         /* tp_iternext */
	0,             /* tp_methods */
	0,             /* tp_members */
	0,                         /* tp_getset */
	0,                         /* tp_base */
	0,                         /* tp_dict */
	0,                         /* tp_descr_get */
	0,                         /* tp_descr_set */
	0,                         /* tp_dictoffset */
	(initproc)math2_py_Primes_init,      /* tp_init */
	0,                         /* tp_alloc */
	0,                 /* tp_new */
};


static PyMethodDef math2Methods[] = {
	{"isprime", math2_py_isPrime, METH_VARARGS, "isprime 's docs"},
	{NULL, NULL, 0, NULL},
};

static struct PyModuleDef math2Module = {
	PyModuleDef_HEAD_INIT,
	"math2",
	NULL,
	-1,
	math2Methods,
};

PyMODINIT_FUNC PyInit_math2(void)
{
	PyObject *m;
	m = PyModule_Create(&math2Module);
	if(m == NULL)
		return NULL;

	math2_py_PrimesType.tp_new = PyType_GenericNew;
	if(PyType_Ready(&math2_py_PrimesType) < 0)
		return NULL;

	Py_INCREF(&math2_py_PrimesType);
	PyModule_AddObject(m, "Primes", (PyObject*)&math2_py_PrimesType);

	return m;
}
