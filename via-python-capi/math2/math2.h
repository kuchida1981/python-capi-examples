#ifndef __math2_h__
#define __math2_h__

#ifdef __cplusplus
extern "C" {
#endif

typedef enum
{
	math2_False = 0,
	math2_True,
} math2_Bool;

struct _math2_Primes
{
	int begin;
	int length;
	int current;
	int returned;
};

typedef struct _math2_Primes math2_Primes;

math2_Bool math2_isPrime(int n);
void math2_Primes_initialize(math2_Primes *pThis, int begin, int length);
math2_Bool math2_Primes_next(math2_Primes *pThis, int *value);

#ifdef __cplusplus
}
#endif
#endif /* __math2_h__ */
