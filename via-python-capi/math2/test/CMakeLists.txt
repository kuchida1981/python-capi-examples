add_subdirectory(${gtest_dir} gtest)

include_directories(${gtest_dir}/include)

set(test_sources math2_test.cpp)

add_executable(math2_test ${test_sources})
target_link_libraries(math2_test gtest gtest_main math2)
