#include <gtest/gtest.h>
#include "../math2.h"

namespace
{
class Math2Test : public ::testing::Test
{
};

TEST_F(Math2Test, math2_isPrime)
{
	ASSERT_TRUE(math2_isPrime(2));
	ASSERT_TRUE(math2_isPrime(3));
	ASSERT_TRUE(math2_isPrime(5));
	ASSERT_TRUE(math2_isPrime(7));
	ASSERT_TRUE(math2_isPrime(11));
	ASSERT_TRUE(math2_isPrime(13));
	ASSERT_TRUE(math2_isPrime(17));
	ASSERT_TRUE(math2_isPrime(19));
	ASSERT_TRUE(math2_isPrime(23));
	ASSERT_TRUE(math2_isPrime(29));
	ASSERT_TRUE(math2_isPrime(31));
}

TEST_F(Math2Test, math2_Primes_next)
{
	int n;
	math2_Bool result;
	math2_Primes *primes;

	primes = new math2_Primes;
	math2_Primes_initialize(primes, 0, 3);

	result = math2_Primes_next(primes, &n);
	ASSERT_TRUE(result);
	ASSERT_EQ(n, 2);

	result = math2_Primes_next(primes, &n);
	ASSERT_TRUE(result);
	ASSERT_EQ(n, 3);

	result = math2_Primes_next(primes, &n);
	ASSERT_TRUE(result);
	ASSERT_EQ(n, 5);

	result = math2_Primes_next(primes, &n);
	ASSERT_FALSE(result);

	delete primes;
}

} /* namespace */
