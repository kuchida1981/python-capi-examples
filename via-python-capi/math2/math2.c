#include "math2.h"


math2_Bool math2_isPrime(int n)
{
	int i;

	if(n < 2)
		return math2_False;
	else if(n == 2)
		return math2_True;

	if(n % 2 == 0)
		return math2_False;

	for(i = 3; i <= n / i; i += 2)
		if(n % i == 0)
			return math2_False;
	return math2_True;
}

void math2_Primes_initialize(math2_Primes *pThis, int begin, int length)
{
	pThis->begin = begin;
	pThis->length = length;
	pThis->current = begin - 1;
	pThis->returned = 0;
}

math2_Bool math2_Primes_next(math2_Primes *pThis, int *value)
{
	if(pThis->returned == pThis->length)
		return math2_False;
	while(1)
	{
		pThis->current += 1;
		if(math2_isPrime(pThis->current))
		{
			*value = pThis->current;
			pThis->returned += 1;
			return math2_True;
		}
	}
}
