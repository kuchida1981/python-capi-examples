import unittest
import math2

class Math2TestCase(unittest.TestCase):

    def test_isprime(self):
        for n in range(32):
            if n in [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31]:
                self.assertTrue(math2.isprime(n))
            else:
                self.assertFalse(math2.isprime(n))

    def test_primes(self):
        self.assertListEqual([2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31],
                list(math2.Primes(0, 11)))
