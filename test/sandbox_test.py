import unittest
import sandbox

class SandboxTestCase(unittest.TestCase):

    def test_summ(self):
        self.assertEqual(sandbox.summ(1, 2, 3, 4), 10)

        self.assertEqual(sandbox.summ(*list(range(10))), 45)

        with self.assertRaises(TypeError):
            self.assertEqual(sandbox.summ('1', '2', '3', '4'), 10)

    def test_map(self):
        self.assertTupleEqual(
                sandbox.map(lambda n: n * 2, (1, 2, 3, 4)),
                (2, 4, 6, 8))

        self.assertTupleEqual(
                sandbox.map(lambda n: n * 2, [1, 2, 3, 4]),
                (2, 4, 6, 8))

        self.assertTupleEqual(
                sandbox.map(lambda m, n: m * n, ((1, 2), (2, 3), (3, 4), (4, 5))),
                (2, 6, 12, 20))
